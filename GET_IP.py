# AUTHOR : G19
# Version : 1.0.0v

import time, socket, os, sys, string


print ("")
print ("########################################################")
print ("#    ##########   ########  ########   ##   ########   #")
print ("#    ##           ##           ##      ##   ##     #   #")
print ("#    ##     ####  ########     ##      ##   ## ####    #")
print ("#    ##      ##   ##           ##      ##   ##         #")
print ("#    ##########   ########     ##      ##   ##         #")
print ("#                                                      #")
print ("#     SCRIPT : GET_IP.PY                               #")
print ("#     AUTHOR : G19                                     #")
print ("#     VERSION: 1.0.0b                                  #")
print ("########################################################")
print ("")



host = raw_input('WEBSITE LINK : ')
ip = socket.gethostbyname(host)
conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 80

def getip():
    try:
        conn.connect((ip, port))
        conn.send("Sending packet confirmation")
    except socket.timeout:   
        print ("")
        print ("#####################################################")
        print ("#     STATUS : DOWN                                 #")
        print ("#     IP: " + ip + "                                #")
        print ("#####################################################")
        print ("")
    print ("")
    print ("#####################################################")
    print ("#     STATUS : UP                                   #")
    print ("#     IP: " + ip + "                                #")
    print ("#####################################################")
    print ("")    
    return

getip()
